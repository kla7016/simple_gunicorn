FROM python:3.7

ADD . /code
WORKDIR /code
RUN pip install -r requirements.txt
EXPOSE 5000
CMD ["gunicorn", "app:app","--config", "/code/gunicorn.config.py"]
